# Diversification of TFF-SF data

This repository contains the HMM profiles and the macsyfinder models that was used in the paper Diversification of the type IV filament super-family into machines for adhesion, secretion, DNA transformation and motility.

This repository contains also the alignments used to infer the different trees of the paper.

To use correctly the model you need to run this command line :

## Initial models (stringent)

For T2SS Tad T4aP ComM

```
macsyfinder -w 4 -d DEF_TFF-SF_stringent/ -p profiles_TFF-SF_stringent/ -o macsyfinder_T2SS-Tad-T4P-T4bP-MSH-ComM --db-type ordered_replicon --sequence-db sequence.fasta T2SS Tad T4P ComM
```

For Archaeal-T4P

```
macsyfinder -w 4 -d DEF_TFF-SF_stringent/ -p profiles_TFF-SF_stringent/ -o macsyfinder_Archaeal-T4P --db-type ordered_replicon --sequence-db sequence.fasta Archaeal-T4P
```

## Final models

For T2SS Tad T4aP T4bP MSH ComM

```
macsyfinder -w 4 -d DEF_TFF-SF_final/ -p profiles_TFF-SF_final/ -o macsyfinder_T2SS-Tad-T4P-T4bP-MSH-ComM --db-type ordered_replicon --sequence-db sequence.fasta T2SS Tad T4P T4bP MSH ComM
```

For Archaeal-T4P

```
macsyfinder -w 4 -d DEF_TFF-SF_final/ -p profiles_TFF-SF_final/ -o macsyfinder_Archaeal-T4P --db-type ordered_replicon --sequence-db sequence.fasta Archaeal-T4P
```

MacSyFinder can be find at : https://github.com/gem-pasteur/macsyfinder
